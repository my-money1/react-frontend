import React from "react";

export default function Footer() {
  return (
    <footer className="main-footer">
      <strong>
        Copyright &copy; 2020
        <a href="http://algoritical.com" target="_blank">
          &nbsp; - Algoritical
        </a>
      </strong>
    </footer>
  );
}
