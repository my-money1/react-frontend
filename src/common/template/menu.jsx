import React from "react";
import MenuItem from "./menuitem";
import MenuTree from "./menutree";

export default function Menu() {
  return (
    <ul className="sidebar-menu">
      <MenuItem path="#/" label="Menu 1" icon="dashboard" />
      <MenuTree label="Cadastro" icon="edit">
        <MenuItem
          path="#billingCycles"
          label="Ciclos de Pagementos"
          icon="usd"
        />
      </MenuTree>
    </ul>
  );
}
